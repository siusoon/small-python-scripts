'''
Python ver 3.8.3 + Mac OS, and also works in ver 2.7.13 + Debian 9

By Winnie Soon and Nynne Lucca

What it does:
- Program to create x amount of crops from y amount of images:
	- Loads all .jpg+.png 's (can be changed to .png's) from current directory and puts them into array
	- Using a while loop to decide how many crops we need
	- Then using a nested for loop to do the actual crop
	- Random function is used to decide where to crop, and how big the crop should be, cf. 'cutSize'
	- Crops saved in folder: 'imgCrops' (make sure to have the imgCrops folder before)

Credit:
See wand documentation here: https://docs.wand-py.org/en/0.6.3/

'''
from __future__ import print_function
from wand.image import Image
import glob
import os
import numpy as np
import random
cutSize_w = 160 #size of crop, width
cutSize_h = 135 #size of crop, height

file_list = glob.glob('*.jpg') + glob.glob('*.png') #get all jpg + png's in current directory

list.sort(file_list)
aImages = np.array(file_list)

i = 1
while i < 2000: #deciding number of crops
    for item in file_list: #loop for each available images
        print (item)
        with Image (filename=item) as img:
            print('width =', img.width)
            print('height =', img.height)
            randomWidth = random.randint(0, img.width-cutSize_w) #using random function to decide where to crop
            randomHeight = random.randint(0, img.height-cutSize_h)
            print(randomWidth)
            img.crop(randomWidth, randomHeight, randomWidth+cutSize_w, randomHeight+cutSize_h)
            img.save(filename='imgCrops/{0}.jpg'.format(i)) #imgs save as 1.jpg, 2.jpg etc. Saves in folder: "imgsCrops
        print(i)
        i += 1
