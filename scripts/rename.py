'''
# this part is about renaming and removing files according to a particular pattern of files

import os
directory = "rename/test/"
files = os.listdir(directory)
counter = 1
incremental = 0

for file in files:
    if file[-4:] == '.tif':
        print file
        if counter == 1:
            os.rename(directory + file, directory + str(incremental).zfill(4) + ".tif")
            incremental +=1
        else:
            os.remove(directory + file)
            if counter == 3:
                counter = 0
        counter += 1
'''
# rename the files with 4 digit incremental count, and it starts with 0026

import os
directory = "rename/JPEG/"
files = os.listdir(directory)
counter = 26

for file in files:
    if file[-4:] == '.jpg':
            os.rename(directory + file, directory + "processed/" + str(counter).zfill(4) + ".jpg")
            counter += 1
